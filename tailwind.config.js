/** @type {import('tailwindcss').Config} */
export default {
  content: ['./*.{html,js}'],
  theme: {
    extend: {
      colors: {
        nutmeg: 'hsl(14, 45%, 36%)',
        eggshell: 'hsl(30, 54%, 90%)',
        'dark-raspberry': 'hsl(332, 51%, 32%)',
        'rose-white': 'hsl(331, 100%, 98%)',
        'light-gray': 'hsl(30, 18%, 87%)',
        'wenge-brown': 'hsl(30, 10%, 34%)',
        'dark-charcoal': 'hsl(24, 5%, 18%)'
      }
    },
    fontFamily: {
      sans: ['Outfit', 'sans-serif'],
      serif: ['Young Serif', 'serif']
    },
    fontSize: {
      h1: '2.5rem',
      h2: '2.25rem',
      h3: '1.75rem',
      h4: '1.25rem'
    },
    screens: {
      md: '768px'
    }
  },
  plugins: []
}
